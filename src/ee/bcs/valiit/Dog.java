package ee.bcs.valiit;
// kommentaar
public class Dog extends LivingThing {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    // kaks kõrvuti stringi saab kirjutada ühe stringina
    public void bark() {
        System.out.println(name + " tegi Auh-Auh! " + "aga ise on " + getType());
    }
}
